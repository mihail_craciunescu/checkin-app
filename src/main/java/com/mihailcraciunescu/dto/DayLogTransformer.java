package com.mihailcraciunescu.dto;

import com.google.common.collect.Maps;
import com.mihailcraciunescu.data.LogEntry;
import com.mihailcraciunescu.util.DateUtil;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

import static com.mihailcraciunescu.data.LogType.CHECK_IN;
import static com.mihailcraciunescu.data.LogType.CHECK_OUT;

/**
 * Created by Mihail on 7/19/2016.
 */
public class DayLogTransformer {

    private final int currentYear;

    public DayLogTransformer(){
        this(LocalDate.now().getYear());
    }

    public DayLogTransformer(int year){
        this.currentYear = year;
    }

    public List<DayLogDTO> build(Iterable<LogEntry> all){
        List<LogEntry> logs = new LinkedList<>();
        all.forEach(l -> logs.add(l));
        Collections.sort(logs, this::getLogComparator);
        List<LogEntry> currentYearLogs = logs.stream().filter(l -> l.getDateTime().getYear() == currentYear).collect(Collectors.toList());
        List<DayLogDTO> list = pairCheckinsIntoDayLogs(currentYearLogs.iterator());
        return list;
    }

    private List<DayLogDTO> pairCheckinsIntoDayLogs(Iterator<LogEntry> logs) {
        List<DayLogDTO> result = new LinkedList<>();
        Map<LocalDate, List<LogEntry>> logsGroup = groupByDay(logs);
        for(LocalDate date: logsGroup.keySet()){
            DayLogDTO dayLogResult = pairCheckinsIntoDayLog(logsGroup.get(date).iterator());
            dayLogResult.setDate(DateUtil.formatDate(date));
            result.add(dayLogResult);
        }
        Collections.sort(result, this::compareDayLogs);
        Collections.reverse(result);
        return result;
    }

    private int compareDayLogs(DayLogDTO d1, DayLogDTO d2){
        return DateUtil.parseDate(d1.getDate()).compareTo(DateUtil.parseDate(d2.getDate()));
    }
    private Map<LocalDate,List<LogEntry>> groupByDay(Iterator<LogEntry> logs) {
        Map<LocalDate, List<LogEntry>> resultMap = new HashMap<>();
        while(logs.hasNext()){
            LogEntry log = logs.next();
            //day change
            if (!resultMap.containsKey(log.getDateTime().toLocalDate())){
                resultMap.put(log.getDateTime().toLocalDate(), new LinkedList<>());
            }
            resultMap.get(log.getDateTime().toLocalDate()).add(log);
        }

        return resultMap;
    }

    private DayLogDTO pairCheckinsIntoDayLog(Iterator<LogEntry> logs){
        DayLogDTO dayLogResult = new DayLogDTO();
        LogEntry checkin = null;
        while (logs.hasNext()){
            LogEntry log = logs.next();
            //find first checkin
            if (checkin == null && CHECK_IN.equals(log.getLogType())){
                checkin = log;
            }else if(checkin != null && CHECK_OUT.equals(log.getLogType())){
                CheckinCheckoutLogDTO checkinCheckout = createCheckinCheckout(checkin, Optional.of(log));
                dayLogResult.getCheckins().add(checkinCheckout);
                checkin =null;
            }
        }

        if (checkin != null && checkin.getDateTime().toLocalDate().equals(LocalDate.now())){
            CheckinCheckoutLogDTO checkinCheckout = createCheckinCheckout(checkin, Optional.empty());
            dayLogResult.getCheckins().add(checkinCheckout);
        }
        //compute total duration
        double tDuration = 0;
        for(CheckinCheckoutLogDTO cin : dayLogResult.getCheckins()){
            tDuration += cin.getDurationInHours();
        }
        dayLogResult.setTotalDuration(tDuration);

        return dayLogResult;
    }

    private CheckinCheckoutLogDTO createCheckinCheckout(LogEntry checkin, Optional<LogEntry> checkout) {
        CheckinCheckoutLogDTO result = new CheckinCheckoutLogDTO();
        result.setCheckinTime(DateUtil.formatTime(checkin.getDateTime().toLocalTime()));
        result.getNotes().add(checkin.getNotes());
        checkout.ifPresent(c -> {
            result.setCheckoutTime(DateUtil.formatTime(c.getDateTime().toLocalTime()));
            result.getNotes().add(c.getNotes());
        });

        LocalDateTime checkoutTime = checkout.isPresent() ? checkout.get().getDateTime() : LocalDateTime.now();
        result.setDurationInHours(checkin.getDateTime().until( checkoutTime, ChronoUnit.MINUTES) / 60.0);

        return result;
    }


    private int getLogComparator(LogEntry t1, LogEntry t2) {
        return t1.getDateTime().compareTo(t2.getDateTime());
    }

}
