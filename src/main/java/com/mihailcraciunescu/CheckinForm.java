package com.mihailcraciunescu;

import com.mihailcraciunescu.data.LogType;
import com.mihailcraciunescu.util.DateUtil;

import java.time.LocalTime;

/**
 * Created by Mihail on 7/18/2016.
 */
public class CheckinForm {

    private String id;
    private LocalTime time = LocalTime.now();
    private String notes = "";
    private String type = LogType.CHECK_IN.name();

    public String getTime() {
        return DateUtil.formatTime(time);
    }

    public void setTime(String time) {
            this.time = DateUtil.parseTime(time);
    }

    public LocalTime getLocalTime(){
        return time;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
