package com.mihailcraciunescu.data;

/**
 * Created by Mihail on 7/18/2016.
 */
public enum LogType {
    CHECK_IN, CHECK_OUT;
}
