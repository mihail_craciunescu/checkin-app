package com.mihailcraciunescu.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by Mihail on 7/19/2016.
 */
public class DateUtil {
    private static DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm");
    private static DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd-MMM-yyyy");
    private static DateTimeFormatter datetimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");

    public static String formatTime(LocalTime time){
        return timeFormatter.format(time);
    }

    public static String formatDate(LocalDate date){
        return dateFormatter.format(date);
    }

    public static String formatDateTime(LocalDateTime dateTime){
        return datetimeFormatter.format(dateTime);
    }

    public static LocalTime parseTime(String time){
        return LocalTime.from(timeFormatter.parse(time));
    }

    public static LocalDate parseDate(String date){
        return LocalDate.from(dateFormatter.parse(date));
    }

    public static LocalDateTime parseDateTime(String dateTime){
        return LocalDateTime.from(datetimeFormatter.parse(dateTime));
    }

}
