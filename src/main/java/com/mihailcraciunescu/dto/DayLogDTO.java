package com.mihailcraciunescu.dto;

import com.mihailcraciunescu.CheckinForm;
import com.mihailcraciunescu.util.DateUtil;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mihail on 7/19/2016.
 */
public class DayLogDTO {

    private String date;
    private double totalDuration;
    private List<CheckinCheckoutLogDTO> checkins = new ArrayList<>();

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<CheckinCheckoutLogDTO> getCheckins() {
        return checkins;
    }

    public void setCheckins(List<CheckinCheckoutLogDTO> checkins) {
        this.checkins = checkins;
    }

    public double getTotalDuration() {
        return totalDuration;
    }

    public void setTotalDuration(double totalDuration) {
        this.totalDuration = totalDuration;
    }

    public String getDay() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("EEEE");
        return dtf.format(DateUtil.parseDate(date));
    }
}
