package com.mihailcraciunescu;

import com.mihailcraciunescu.data.LogEntry;
import com.mihailcraciunescu.data.LogEntryRepository;
import com.mihailcraciunescu.data.LogType;
import com.mihailcraciunescu.dto.DayLogDTO;
import com.mihailcraciunescu.dto.DayLogTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class CheckinController {

    @Autowired
    private LogEntryRepository logEntryRepository;

    @RequestMapping(value = "/checkin" , method = RequestMethod.POST)
    public LogEntry checkin(@ModelAttribute CheckinForm checkinForm, Model model) {
        model.addAttribute("name", checkinForm.getTime());
        LogEntry logEntry = new LogEntry();
        LocalDateTime dateTime = LocalDateTime.of(LocalDate.now(),checkinForm.getLocalTime());
        logEntry.setDateTime(dateTime);
        logEntry.setLogType(LogType.valueOf(checkinForm.getType()));
        logEntry.setNotes(checkinForm.getNotes());
        logEntryRepository.save(logEntry);
        return logEntry;
    }

    @RequestMapping(value = "/updateCheckin" , method = RequestMethod.POST)
    public LogEntry updateCheckin(@ModelAttribute EditCheckinForm editCheckinForm, Model model) {
        model.addAttribute("name", editCheckinForm.getTime());
        LogEntry logEntry = logEntryRepository.findOne(editCheckinForm.getId());
        logEntry.setDateTime(editCheckinForm.getLocalDateTime());
        logEntry.setLogType(LogType.valueOf(editCheckinForm.getType()));
        logEntry.setNotes(editCheckinForm.getNotes());
        logEntryRepository.save(logEntry);
        return logEntry;
    }

    @RequestMapping(value = "/dayLogs" , method = RequestMethod.GET)
    public List<DayLogDTO> dayLogs() {
        List<DayLogDTO> result = new DayLogTransformer().build(logEntryRepository.findAll());
        return result.stream().limit(100).collect(Collectors.toList());
    }

    @RequestMapping(value = "/dayLogs" , method = RequestMethod.POST)
    public List<DayLogDTO> dayLogs(@RequestParam String startDate,@RequestParam String endDate) {
        List<DayLogDTO> result = new DayLogTransformer().build(logEntryRepository.findAll());
        return result.stream().limit(100).collect(Collectors.toList());
    }


}
