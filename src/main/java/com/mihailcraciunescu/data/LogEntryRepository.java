package com.mihailcraciunescu.data;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by Mihail on 7/18/2016.
 */
@RepositoryRestResource(collectionResourceRel = "logs", path = "logs")
public interface LogEntryRepository extends CrudRepository<LogEntry, String> {
    List<LogEntry> findByLogType(@Param("logType") LogType logType);
}
