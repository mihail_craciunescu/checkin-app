$(".clockpicker").clockpicker({
    placement: 'right',
    align: 'top',
    autoclose: true
});

function checkin(){
    console.log('[debug] checkin');
    $("#checkinType").val("CHECK_IN");
    submitForm($('#checkin-form'),function(logEntry){location.reload();});
     return false;
}

function checkout(){
    console.log('[debug] checkout');
    $("#checkinType").val("CHECK_OUT");
    submitForm($('#checkin-form'),function(logEntry){location.reload();});
    return false;
}

jQuery(document).ready(function($) {
		$("#checkin-form").submit(function(event) {
			// Prevent the form from submitting via the browser.
			event.preventDefault();
			submitForm($('#checkin-form'),function(logEntry){location.reload();});
		});
		$("#checkin-edit-form").submit(function(event) {
            // Prevent the form from submitting via the browser.
            event.preventDefault();
            submitForm($('#checkin-edit-form'),function(logEntry){location.reload();});
        });
        reformatLogTime();
	});

function reformatLogTime(){
    $("#dailyLog td.logTime").each(
        function(index,item){
            var logTime = $(item).text().toHHMM();
            console.log(index + " : " + logTime);
            $(item).text(logTime);
        });
}

function submitForm($form, successFunction) {
    console.log('[debug] submitting form: '+$form.serialize());

	$.ajax({
      url: $form.attr('action'),
      type: 'post',
      data: $form.serialize(),
      error:function(response){alert("Error submitting form! ");console.log("[debug] response:" + response)},
      success: function(response) {
        if ($(response).find('.has-error').length) {
          $form.replaceWith(response);
          console.log('[debug] error submit: '+response);
        } else {
          $("#submit-result").val("Form submitted successfully.");
          console.log('[debug] success submit: '+response);
          successFunction(response);
        }
      }
  });
}

$('#logs').bootstrapTable({
    onClickRow: function (row, element) {
        console.log('[debug] row click: '+row.id + " elem: " + element);
        fillForm(row);
    }
});

function fillForm(row){
    var form = $('#checkin-edit-form');
    form.find('input[name=id]').val(row.id);
    form.find('input[name=dateTime]').val(row.time);
    form.find('select[name=type]').val(row.type);
    form.find('input[name=notes]').val(row.notes);
    console.log('[debug] fill form with entry: '+row.id +" - " + row.time);
}
function saveLog(){
    var logId = $('#checkin-edit-form-id').val();
    console.log('[debug] saving entry: '+logId);
    submitForm($('#checkin-edit-form'),function(logEntry){location.reload();});
  return false;
}
function deleteLog(){
    var logId = $('#checkin-edit-form-id').val();
    console.log('[debug] deleting entry: '+logId);
    $.ajax({
        url: "/logs/" + logId,
        type: "DELETE",
        error:function(response){alert("Error detleting entry! ");},
        success: function(response) {
            if ($(response).find('.has-error').length) {
              alert("Error detleting entry!");
              console.log('[debug] error submit: '+response);
            } else {
              alert("Entry deleted: " + logId);
              console.log('[debug] success delete: '+response);
              location.reload();
            }
        }
    });
     return false;
}
String.prototype.toHHMM = function () {
    var hVal = parseFloat(this); // don't forget the second param
    var hours   = Math.floor(hVal);
    var minutes = Math.floor((hVal - hours) * 60);

    if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    return hours+"h "+minutes+" min";
}