package com.mihailcraciunescu;

import com.mihailcraciunescu.data.LogEntry;
import com.mihailcraciunescu.data.LogEntryRepository;
import com.mihailcraciunescu.dto.DayLogDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Mihail on 7/19/2016.
 */
@Controller
public class StartController {

    @Autowired
    private LogEntryRepository logEntryRepository;

    @Autowired
    private CheckinController checkinController;

    @RequestMapping("/start")
    public String start( Model model) {
        model.addAttribute("checkinForm", new CheckinForm());
        model.addAttribute("checkinEditForm", new EditCheckinForm());
        List<LogEntry> currentLogs = new ArrayList<>();
        logEntryRepository.findAll().forEach(l -> currentLogs.add(l));
        Collections.sort(currentLogs, (l1, l2)->l2.getDateTime().compareTo(l1.getDateTime()));
        model.addAttribute("currentLogs",currentLogs);
        return "start";
    }

    @RequestMapping("/reports")
    public String reports( Model model) {
        List<DayLogDTO> dayLogDTOs = checkinController.dayLogs();
        model.addAttribute("dailyLogs",dayLogDTOs);
        return "reports";
    }
}
