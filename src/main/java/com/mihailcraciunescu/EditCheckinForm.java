package com.mihailcraciunescu;

import com.mihailcraciunescu.util.DateUtil;

import java.time.LocalDateTime;

/**
 * Created by Mihail on 7/18/2016.
 */
public class EditCheckinForm extends CheckinForm{

    private LocalDateTime dateTime = LocalDateTime.now();

    public LocalDateTime getLocalDateTime() {
        return dateTime;
    }

    public String getDateTime() {
        return DateUtil.formatDateTime(dateTime);
    }
    public void setDateTime(String dateTime) {
        this.dateTime = DateUtil.parseDateTime(dateTime);
    }
}
